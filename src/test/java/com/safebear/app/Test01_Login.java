package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {


    @Test
    public void testLogin() {
        //Step 1 Confirm were on the Welcome Page
        assertTrue(welcomePage.checkCorrrectPage());
        //Step2 Click on the login link and Login Page loads
        welcomePage.clickOnLogin();
        //Step 3 Check on login
        assertTrue(loginPage.checkCorrectPage());
        //Step 4 Login
        loginPage.login("testuser","testing" );
        //Step 5 confirms user page
        assertTrue(userPage.checkPage());
    }
} //end code